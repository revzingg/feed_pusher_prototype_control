all:
	gcc -o main.o main.c -c
	gcc -o feed_pusher_joystick_control main.o -lm -lSDL2
	sudo cp feed_pusher_joystick_control /usr/bin/feed_pusher_joystick_control
	rm -rf *.o
	rm feed_pusher_joystick_control
