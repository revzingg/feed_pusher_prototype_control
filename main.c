/* ----------------------------------------------------------------------------
 * Управление тележкой разнотягом с джойстика.
 * Левый стик вверх/вниз -> вперёд/назад,
 * правый стик влево/вправо -> "руль" влево/вправо,
 * LT1, LT2 -- опустить, поднять ковш.
 * авторы Рекун, Ревзин
 * к feed_pusher_v0
 * в etc/udev/rules.d нужно создать правило и в нём написать:
 * SUBSYSTEMS=="usb", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", GROUP="users", MODE="0666", SYMLINK="ttyUSBUART"
 ----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

#include <SDL2/SDL.h>

#include "zed_dbg.h"

#define PI 3.14159265
#define JOY_MAX 32768.0

#define MAX_THROTTLE 30
#define MAX_ANGLE 20.0
#define DIFF_QUOTA 0.7
// Half car length
#define LINKLENGTH 0.8
#define TRACKWIDTH 1.0

struct _chassis_params
{
    int direction_control_flip; /* is the forward direction flipped? */
    int blade_control_flip;     /* is the blade control flipped? */
} chassis_params = {0};

struct _joy_input
{
    float throttle;
    float rotation;
    uint8_t raise;
    uint8_t lower;
};

struct _packet
{
    uint8_t head;           /* always 0x14 */
    uint8_t l_direction;    /* 0x00 -- fwd, 0x01 -- bwd */
    uint8_t l_throttle;     /* 0 - 25 speed */
    uint8_t r_direction;    /* 0x00 -- fwd, 0x01 -- bwd */
    uint8_t r_throttle;     /* 0 - 25 speed */
    uint8_t blade;          /* 0x00 -- blade stationary,
                                0x01 -- blade goes up,
                                0x02 -- blade goes down */
    uint8_t end;            /* always 0x88 */
};

struct _rx_packet
{
    uint8_t head;
    uint8_t tail;
};

struct _diff
{
    float l_throttle;
    float r_throttle;
};

typedef enum {
    STATE_OK,
    STATE_HOST_TTY_NOT_READY,
    STATE_HOST_NO_JOYSTICKS,
    STATE_HW_NO_RESPONSE
} e_ctl_state;

/* tty setup */
void port_setup();

/* calcualtes the 'gear' differential with get_diff and constucts a
 * packet for the hardware */
void packet_construct(struct _joy_input *jinput, struct _packet *packet,
                                struct _chassis_params *_ch_par,
                                struct _diff (*get_diff)(float, float));

/* not serious not physical differential */
struct _diff naive_diff(float throttle, float rotation);

/* serious physical differential */
struct _diff phys_diff(float throttle, float rotation);

/* logs the packet to FILE */
void log_packet(FILE *f, struct _packet *p);

/* sends a packet to the port */
int port_send(struct _packet packet);

/* receives count bytes from port port into whereto */
int port_receive_bytes(int port, int count, uint8_t *whereto);

/*  returns 0 if a correct confirmation (0x14, 0x88) is read
    back from hardware */
int port_receive_confirmation(int port);

/* delays before a retry */
void delay_retry();

/* returns 1 if file exists */
int file_ready(char *name);

/* prints what happens now */
void print_status(e_ctl_state state, char *string);

/* returns 0 if ok */
int start_sdl();

/* returns the pointer to a joystick */
SDL_Joystick *open_sdl_joystick();

/* processes sdl events and populates the jinput */
int process_sdl_events(struct _joy_input *jinput);

/* tries to close the descriptor */
void port_try_close(int descr);

/* will return the file descriptor or -1 */
int port_configure(char *device_name);

/* returns number of read bytes; -1 if timeout, -2 if i/o error */
int port_read_bytes(int descriptor, size_t count,
                                        uint8_t *bytes, int timeout_s);

/* returns number of written bytes; -1 if timeout, -2 if i/o error */
int port_write_bytes(int descriptor, size_t count, uint8_t *bytes,
                                                        int timeout_s);

/* tries to shit out a stop packet no matter what */
int scream_stop_packet(int port_descriptor);

int main(int argc, char **argv)
{
    printf("%s\n", "Feed Pusher control startup...");
    printf("%s\n", "Please add the following udev rule");
    printf("%s\n\n", "\t (in /etc/udev/rules.d/):");
    printf("%s\n", "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"10c4\", "
                    "ATTRS{idProduct}==\"ea60\", GROUP=\"users\", "
                    "MODE=\"0666\", SYMLINK=\"ttyUSBUART\"");
    printf("\n\n");



    char *dev_name;

    if (argc > 0)
        dev_name = argv[1];
    else
        dev_name = "/dev/ttyUSBUART";

    printf("Using %s as tty\n", dev_name);

    int port_descriptor = -1;

    e_ctl_state state = STATE_HOST_TTY_NOT_READY;

    struct _packet packet = {0x00};
    packet.head = 0x14;
    packet.end = 0x88;

    struct _joy_input input = {0x00};

    SDL_Joystick *joy;

    check(!start_sdl(), "SDL failure: exiting");

    for(;;) {
        print_status(state, NULL);

        if (state > STATE_HOST_NO_JOYSTICKS) {
            /* get the imputs */
            int rc = process_sdl_events(&input);

            if (rc == 1) {
                printf("Quit signal! Sending a stop packet...");
                scream_stop_packet(port_descriptor);
                exit(1);
            }

            /* prepare the packet */
            packet_construct(&input, &packet, &chassis_params, naive_diff);
        }

        switch (state) {
        case STATE_HOST_TTY_NOT_READY:
        {
            if (file_ready(dev_name)) {
                port_try_close(port_descriptor);
                port_descriptor = port_configure(dev_name);
                if (port_descriptor <= 0)
                    delay_retry();
                else
                    state = STATE_HOST_NO_JOYSTICKS;
            }
            else
                delay_retry();

            break;
        }
        case STATE_HOST_NO_JOYSTICKS:
        {
            if ((joy = open_sdl_joystick()))
                state = STATE_HW_NO_RESPONSE;
            else {
                if (-1 == scream_stop_packet(port_descriptor))
                    state = STATE_HOST_TTY_NOT_READY;
                else
                    delay_retry();
            }
            break;
        }

        case STATE_HW_NO_RESPONSE:
        {
            if (SDL_NumJoysticks() < 1) {
                scream_stop_packet(port_descriptor);
                state = STATE_HOST_NO_JOYSTICKS;
                break;
            }

            log_packet(stdout, &packet);
            int rc = port_write_bytes(port_descriptor, sizeof(struct _packet),
                                                        (uint8_t *) &packet, 1);
            if (rc == -1) {
                state = STATE_HW_NO_RESPONSE;
            }
            else if (rc == -2)
                state = STATE_HOST_TTY_NOT_READY;

            rc = port_receive_confirmation(port_descriptor);

            if (rc == -1) {
                scream_stop_packet(port_descriptor);
                state = STATE_HW_NO_RESPONSE;
                delay_retry();
                break;
            }
            else if (rc == -2) {
                state = STATE_HOST_TTY_NOT_READY;
                delay_retry();
                break;
            }


            break;
        }
        case STATE_OK:
        {
            log_packet(stdout, &packet);
            int rc = port_write_bytes(port_descriptor, sizeof(struct _packet),
                                                        (uint8_t *) &packet, 1);
            /* check write didn't fail; if write fails, something wrong with tty */
            if (rc < 0) {
                state = STATE_HOST_TTY_NOT_READY;
                break;
            }

            rc = port_receive_confirmation(port_descriptor);

            if (rc == -1) {
                scream_stop_packet(port_descriptor);
                state = STATE_HW_NO_RESPONSE;
                delay_retry();
                break;
            }
            else if (rc == -2) {
                state = STATE_HOST_TTY_NOT_READY;
                delay_retry();
                break;
            }

            break;
        }
        default:
        {
            debug("Internal error, invalid state=%d", state);
            goto error;
        }
        }
    }


    return 0;

    error:
        return -1;
}

void log_packet(FILE *f, struct _packet *p)
{
    uint8_t *body = (uint8_t *) p;
    int i;
    for (i = 0; i < sizeof(struct _packet); i++)
        fprintf(f, "0x%02x ", body[i]);
    fprintf(f, "\n\r");
    fflush(f);
}


struct _diff naive_diff(float throttle, float rotation)
{
    struct _diff diff_out;

    float inner = fabs(throttle) * (1.0 - DIFF_QUOTA);
    float outer = inner * ( 1 + (fabs(rotation) * DIFF_QUOTA) );

    diff_out.l_throttle = rotation > 0 ? outer : inner;
    diff_out.r_throttle = rotation > 0 ? inner : outer;
    return diff_out;
}


struct _diff phys_diff(float throttle, float rotation)
{
    struct _diff diff_out;

    if (rotation == 0) {
        diff_out.l_throttle = fabs(throttle);
        diff_out.r_throttle = fabs(throttle);
        return diff_out;
    }
    float rot_angle = (fabs(rotation) * MAX_ANGLE);
    float alpha = (180 - rot_angle) / 2;
    float radius = LINKLENGTH / (2 * cos(alpha*PI/180));
    check(TRACKWIDTH <= radius, "TRACKWIDTH must be smaller than turn radius"
                                " - increase LINKLENGTH, decrease TRACKWIDTH");

    float W = fabs(throttle) / radius;

    float inner = W * (radius - TRACKWIDTH / 2);
    float outer = W * (radius + TRACKWIDTH / 2);

    diff_out.l_throttle = rotation > 0 ? outer : inner;
    diff_out.r_throttle = rotation > 0 ? inner : outer;
    return diff_out;

    error:
        exit(3);
}


/* calcualtes the 'gear' differential with get_diff and constucts a
 * packet for the hardware */
void packet_construct(struct _joy_input *jinput, struct _packet *packet,
                                struct _chassis_params *_ch_par,
                                struct _diff (*get_diff)(float, float))
{
    if (!_ch_par->direction_control_flip)
        packet->r_direction = packet->l_direction = ( jinput->throttle < 0 );
    else
        packet->r_direction = packet->l_direction = ( jinput->throttle > 0 );

    if (!_ch_par->blade_control_flip)
        packet->blade = jinput->raise == jinput->lower ? 0
                            : jinput->raise + jinput->lower * 2;
    else
        packet->blade = jinput->raise == jinput->raise ? 0
                            : jinput->raise + jinput->lower * 2;

    struct _diff diff_res = get_diff(jinput->throttle, jinput->rotation);
    packet->l_throttle = (uint8_t)(diff_res.l_throttle * MAX_THROTTLE);
    packet->r_throttle = (uint8_t)(diff_res.r_throttle * MAX_THROTTLE);
}

int port_receive_confirmation(int port)
{
    struct _rx_packet rx_packet = {0x00};

    int rc = port_read_bytes(port, sizeof(struct _rx_packet),
                                                    (uint8_t *) &rx_packet, 1);
    if (rc == -1)
        return -1;
    else if (rc == -2)
        return -2;
    else if (rc != 2 || (!(rx_packet.head == 0x14 && rx_packet.tail == 0x88))) {
            printf("%s\n", "Unexpected hardware response:");
            printf("0x%02x 0x%02x\n", rx_packet.head, rx_packet.tail);
            return -1;
        }
    return 0;
}

void delay_retry()
{
    SDL_Delay(500);
}

/* returns 1 if file exists */
int file_ready(char *name)
{
    if (access(name, F_OK)  != -1)
        return 1;
    else
        return 0;
}

void print_status(e_ctl_state state, char *string)
{
    static unsigned int tick = 0;

    static const char *status_strings[] = {
                        "HW response OK, continuing...",
                        "Trying to init TTY...",
                        "Trying to connect to a joystick...",
                        "Trying to connect to hardware..."};

    check(state >= STATE_OK && state <= STATE_HW_NO_RESPONSE,
                    "Internal error: invalid state=%d", state);

    if (string == NULL)
        printf("[%d] %s\n", tick, status_strings[state]);
    else
        printf("%s\n", string);
    fflush(stdout);
    tick++;
    return;

    error:
        exit(2);
}

/* returns 0 if ok */
int start_sdl()
{
    SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");
    if( SDL_Init(SDL_INIT_TIMER | SDL_INIT_JOYSTICK) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }
    SDL_JoystickEventState(SDL_ENABLE);
    return 0;
}

/* returns the pointer to a joystick */
SDL_Joystick *open_sdl_joystick()
{
    SDL_JoystickUpdate();
    SDL_Joystick *joy = NULL;
    if( SDL_NumJoysticks() < 1 )
        printf( "Warning: No joysticks connected!\n" );

    joy = SDL_JoystickOpen( 0 );
    check(joy, "Failed to open the joystick, SDL error: %s", SDL_GetError());
    return joy;

    error:
    return NULL;
}

int process_sdl_events(struct _joy_input *jinput)
{
    SDL_Event e;
    while ( SDL_PollEvent(&e) ) {
        switch(e.type)  {
        case SDL_JOYAXISMOTION:
            if ( e.jaxis.axis == 1 )
                jinput->throttle = -e.jaxis.value / JOY_MAX;
            if ( e.jaxis.axis == 2 )
                jinput->rotation = e.jaxis.value / JOY_MAX;
            break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
            if ( e.jbutton.button == 4 )
                jinput->raise = e.jbutton.state;
            if ( e.jbutton.button == 6 )
                jinput->lower = e.jbutton.state;
            break;
        case SDL_QUIT:
            exit(1);
        }
    }
    return 0;
}


void port_try_close(int descr)
{
    close(descr);
}


/* will return the file descriptor or -1 */
int port_configure(char *device_name)
{
    int s_port = open(device_name, O_RDWR);
    check(s_port > 0, "failed to open device %s", device_name);

    struct termios tty;
    memset (&tty, 0, sizeof(tty));

    /* Error Handling */
    check(!tcgetattr ( s_port, &tty ), "tcgetattr failed");

    /* Set Baud Rate */
    cfsetospeed (&tty, (speed_t)B9600);
    cfsetispeed (&tty, (speed_t)B9600);

    /* Setting other Port Stuff */
    tty.c_cflag     &=  ~PARENB;            // Make 8n1
    tty.c_cflag     &=  ~CSTOPB;
    tty.c_cflag     &=  ~CSIZE;
    tty.c_cflag     |=  CS8;

    tty.c_cflag     &=  ~CRTSCTS;           // no flow control
    tty.c_cc[VMIN]   =  1;                  // read doesn't block
    tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
    tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

    /* Make raw */
    cfmakeraw(&tty);

    /* Flush Port, then apply attributes */
    tcflush( s_port, TCIFLUSH );
    check(!tcsetattr ( s_port, TCSANOW, &tty ), "tcsetattr failed");

    return s_port;

    error:
        return -1;

}

int port_read_bytes(int descriptor, size_t count,
                                        uint8_t *bytes, int timeout_s)
{
    int nread = 0;

    check(timeout_s > 0, "invalid timeout_s value (must be > 0)");
    check(count > 0, "cannot read zero bytes!");
    check(fcntl(descriptor, F_GETFD) != -1 || errno != EBADF,
                                                "bad file descriptor");

    fd_set read_fds, write_fds, except_fds;
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);
    FD_ZERO(&except_fds);
    FD_SET(descriptor, &read_fds);

    struct timeval timeout;
    timeout.tv_sec = timeout_s;
    timeout.tv_usec = 0;

    if (select(descriptor + 1, &read_fds, &write_fds,
                &except_fds, &timeout) == 1)
    {
        /* device is ready */
        int rc = 0;
        nread = 0;
        while (nread != count) {
            rc = read(descriptor, bytes + (long) nread, count - nread);
            check(rc > 0, "read() failed!");
            nread += rc;
            check(nread <= count, "read more bytes than expected");
        }
    }
    else
        return -1;

    return nread;

    error:
        return -2;
}

int port_write_bytes(int descriptor, size_t count, uint8_t *bytes,
                                                        int timeout_s)
{
    int written = 0;

    check(timeout_s > 0, "invalid timeout_s value");
    check(count > 0, "cannot write zero bytes!");

    fd_set read_fds, write_fds, except_fds;
    FD_ZERO(&read_fds);
    FD_ZERO(&write_fds);
    FD_ZERO(&except_fds);
    FD_SET(descriptor, &write_fds);

    struct timeval timeout;
    timeout.tv_sec = timeout_s;
    timeout.tv_usec = 0;

    if (select(descriptor + 1,
                &read_fds, &write_fds, &except_fds, &timeout) == 1)
    {
        /* device is ready */
        int rc = 0;
        written = 0;
        while (written != count) {
            rc = write(descriptor, bytes + (long) written, count - written);
            check(rc > 0, "write() failure")
            written += rc;
            check(written <= count, "write() wrote more than requested!");
        }
    }
    else
        return -1;

    return written;

    error:
        return -2;
}


int scream_stop_packet(int port_descriptor)
{
    struct _packet stop_packet = {0x00};
    stop_packet.head = 0x14;
    stop_packet.end = 0x88;
    check(-2 != port_write_bytes(port_descriptor, sizeof(struct _packet),
                                        (uint8_t *) &stop_packet, 1),
                                        "port_write_bytes() failed");
    printf("Stop packet screamed...\n");
    return 0;

    error:
    return -1;
}
